#
# Copyright (C) 2020 The Android Open-Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

PRODUCT_MODEL := Total Compute Fast Model (S/W rendering build)

DEVICE_MANIFEST_FILE += $(LOCAL_PATH)/manifest/no_mali/manifest_vintf.xml

PRODUCT_PACKAGES += \
    \
    android.hardware.drm@1.3-impl \
    android.hardware.drm@1.3-service \
    \
    android.hardware.graphics.composer@2.4-service \
    android.hardware.graphics.composer@2.4-impl \
    android.hardware.graphics.allocator@4.0-service.minigbm \
    android.hardware.graphics.mapper@4.0-impl.minigbm \
    \
    gralloc.minigbm \
    hwcomposer.drm_minigbm \
    libEGL_angle \
    libGLESv1_CM_angle \
    libGLESv2_angle \
    vulkan.pastel \
    \
    tipc-test

PRODUCT_PROPERTY_OVERRIDES += \
    ro.hardware.gralloc=minigbm \
    ro.hardware.hwcomposer=drm_minigbm

PRODUCT_VENDOR_PROPERTIES += \
    ro.hardware.egl=angle \
    ro.hardware.vulkan=pastel

PRODUCT_COPY_FILES += \
    $(LOCAL_PATH)/rc_script/init.no_mali.rc:root/init.total_compute.rc
