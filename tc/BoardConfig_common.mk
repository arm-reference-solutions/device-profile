#
# Copyright (C) 2019-2023 The Android Open-Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

BUILD_BROKEN_MISSING_REQUIRED_MODULES := true
BOARD_VNDK_VERSION := current

TARGET_NO_BOOTLOADER := true
TARGET_NO_KERNEL := true
TARGET_NO_RECOVERY := true

TARGET_ARCH := arm64
TARGET_ARCH_VARIANT := armv8-a-branchprot
TARGET_CPU_VARIANT := generic
TARGET_CPU_ABI := arm64-v8a

BOARD_HAVE_BLUETOOTH := false

# Include configuration of partitions.
include device/arm/tc/BoardConfig-partitions.mk

# Do not enable AVB for eng builds, only for userdebug and user builds
ifneq ($(TARGET_BUILD_VARIANT),eng)
BOARD_AVB_ENABLE := true
endif

ifeq (${TC_GPU},true)
BOARD_SEPOLICY_DIRS += device/arm/tc/sepolicy/mali
else
BOARD_SEPOLICY_DIRS += device/arm/tc/sepolicy/no_mali
endif
