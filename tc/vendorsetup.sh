#!/bin/bash
#
# Copyright (c) 2020-2023, ARM Ltd.
# All rights reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

function apply_patch {
  pushd .
  cd $1
  git am $PATCHES_DIR/$2 >/dev/null 2>&1
  if [ $? -eq 0 ];
  then
    echo "Applied $2"
  else
    echo "Patch am failed, Either patch did not apply cleanly or was already applied." \
         "Aborting git am."
    git am --abort
  fi
  popd
}

ANDROID_TOP_DIR=$(pwd)

PATCHES_DIR=$ANDROID_TOP_DIR/device/arm/tc/patches/
apply_patch "system/unwinding" "0001-libbacktrace-build-fix.patch"
apply_patch "external/drm_hwcomposer/" "0001-external-drm_hwcomposer-avoid-USE_IMAPPER4_METADATA_.patch"
apply_patch "external/drm_hwcomposer/" "0001-drm_hwcomposer-Allow-more-than-1-primary-plane.patch"
# Patch to copy trusty tipc-test in /vendor/bin path
apply_patch "system/core" "0001-trusty-tipc-test-copy-in-bin-path.patch"
apply_patch "packages/modules/Virtualization" "0002-Add-Microdroid-demo-app-for-TC.patch"
