#
# Copyright (C) 2023 The Android Open-Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

PRODUCT_MODEL := Total Compute (H/W rendering build)

GRALLOC_DISABLE_FRAMEBUFFER_HAL := 1
GRALLOC_HWC_FORCE_BGRA_8888 := 1
GRALLOC_HWC_FB_DISABLE_AFBC := 1

GRALLOC_FB_SWAP_RED_BLUE := 1
GRALLOC_DEPTH := GRALLOC_32_BITS

# Include board specific mali configuration files if they exist.
# They won't exist when building with MALI_DISABLED.
-include vendor/arm/mali/gpu/product/android/gralloc/gralloc.device.mk
-include vendor/arm/mali/product/android/gralloc/gralloc.device.mk
-include vendor/arm/mali/gpu/product/android/renderscript/renderscript.device.mk
-include vendor/arm/mali/product/android/renderscript/renderscript.device.mk
-include vendor/arm/mali/gpu/product/mali_gpu.mk
-include vendor/arm/mali/product/mali_gpu.mk
-include vendor/arm/mali/gpu/product/android/sepolicies/sepolicy.mk
-include vendor/arm/mali/product/android/sepolicies/sepolicy.mk

# ARM RenderScript driver
OVERRIDE_RS_DRIVER := libRSDriverArm.so

# The graphics allocator, composer and mapper HAL interfaces are now optionally specified in the latest versions
# of the gralloc module. We will call this a VINTF_ENABLED gralloc submodule.
# These HAL interfaces must be declared somewhere for a build to succeed and they must not be declared twice.
# For this reason a VINTF_ENABLED gralloc submodule will detect the existence of "manifest_vintf.xml" in the BSP and
# will in turn generate a "vintf_enabled" file. If the vintf_enabled file is not detected then manifest_vintf.xml will
# be added to the DEVICE_MANIFEST_FILE.
ifeq ($(or $(wildcard vendor/arm/mali/gpu/product/android/gralloc/build_vintf/vintf_enabled), $(wildcard vendor/arm/mali/product/android/gralloc/build_vintf/vintf_enabled)),)
  DEVICE_MANIFEST_FILE += $(LOCAL_PATH)/manifest/mali/manifest_vintf.xml

  PRODUCT_PACKAGES += \
        android.hardware.graphics.allocator@4.0-impl-arm \
        android.hardware.graphics.allocator@4.0-service \
        android.hardware.graphics.allocator@4.0.vndk-sp \
        android.hardware.graphics.composer@2.2-impl \
        android.hardware.graphics.composer@2.2-service \
        android.hardware.graphics.mapper@4.0-impl-arm \
        android.hardware.graphics.mapper@4.0.vndk-sp
endif

ifeq ($(TARGET_HWCOMPOSER_DISABLE_BUILD),)
PRODUCT_PACKAGES += hwcomposer.drm_mappermetadata
endif


# Avoids some warnings in surfaceflinger.
PRODUCT_PROPERTY_OVERRIDES += \
    ro.hardware.hwcomposer=drm_mappermetadata


PRODUCT_PACKAGES += \
    libvulkan vkinfo

PRODUCT_PROPERTY_OVERRIDES += \
    ro.hardware.egl=mali

PRODUCT_PROPERTY_OVERRIDES += ro.opengles.version=196610

PRODUCT_COPY_FILES += \
    $(LOCAL_PATH)/rc_script/init.mali.rc:root/init.total_compute.rc
