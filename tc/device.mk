#
# Copyright (C) 2020-2023 The Android Open-Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

PRODUCT_FULL_TREBLE_OVERRIDE := true
PRODUCT_OTA_ENFORCE_VINTF_KERNEL_REQUIREMENTS := false
PRODUCT_PRODUCT_VNDK_VERSION := current

PRODUCT_SOONG_NAMESPACES += device/generic/goldfish

DEVICE_MANIFEST_FILE := $(LOCAL_PATH)/manifest/manifest_common.xml

ifeq (${TC_GPU},false)
    DEVICE_MANIFEST_FILE += $(LOCAL_PATH)/manifest/no_mali/manifest_vintf.xml
endif

$(call inherit-product-if-exists,frameworks/native/build/phone-xhdpi-2048-dalvik-heap.mk)
$(call inherit-product, $(SRC_TARGET_DIR)/product/core_64_bit.mk)
$(call inherit-product, $(SRC_TARGET_DIR)/product/full_base.mk)
$(call inherit-product, system/core/trusty/trusty-test.mk)
$(call inherit-product, packages/modules/Virtualization/apex/product_packages.mk)

MULTILIB_BUILD := false

PRODUCT_SHIPPING_API_LEVEL := 29
PRODUCT_OTA_ENFORCE_VINTF_KERNEL_REQUIREMENTS := false
PRODUCT_ENFORCE_VINTF_MANIFEST_OVERRIDE := true
DEVICE_PACKAGE_OVERLAYS := device/arm/tc/overlay/

DISABLE_RILD_OEM_HOOK := true

# Boot image is only built in user and userdebug modes, In these modes,
# the kernel Image is copied to device profile and used to build the boot image.
# The boot image is signed during Android Verified Boot process.
ifneq ($(TARGET_BUILD_VARIANT),eng)
PRODUCT_COPY_FILES += \
    $(LOCAL_PATH)/Image:kernel
endif

PRODUCT_PACKAGES += \
    android.hardware.keymaster@4.0-service \
    android.hardware.keymaster@4.0-impl \
    \
    android.hardware.gatekeeper@1.0-service.software \
    \
    android.hardware.broadcastradio@1.0-impl \
    android.hardware.thermal@2.0-service.mock
    
PRODUCT_PACKAGES += \
    android.hardware.audio.service \
    android.hardware.audio@7.1-impl \
    android.hardware.soundtrigger@2.3-impl \
    android.hardware.audio.effect@7.0-impl

PRODUCT_PACKAGES += \
    health.default \
    android.hardware.health \
    android.hardware.health-service.example

PRODUCT_PACKAGES += \
    android.hardware.power-service.example \
    android.hardware.power.stats-service.example

PRODUCT_PACKAGES += \
    disable_configstore

DEVICE_MANIFEST_FILE += device/generic/goldfish/audio/android.hardware.audio.effects@7.0.xml

PRODUCT_PACKAGES += \
	android.hardware.power-service.example

# Audio Policy Configurations
FW_AV_AUDIOPOLICY_CFG_PATH := frameworks/av/services/audiopolicy/config
PRODUCT_COPY_FILES += \
    $(FW_AV_AUDIOPOLICY_CFG_PATH)/audio_policy_configuration_generic.xml:$(TARGET_COPY_OUT_VENDOR)/etc/audio_policy_configuration.xml \
    $(FW_AV_AUDIOPOLICY_CFG_PATH)/primary_audio_policy_configuration.xml:$(TARGET_COPY_OUT_VENDOR)/etc/primary_audio_policy_configuration.xml \
    $(FW_AV_AUDIOPOLICY_CFG_PATH)/r_submix_audio_policy_configuration.xml:$(TARGET_COPY_OUT_VENDOR)/etc/r_submix_audio_policy_configuration.xml \
    $(FW_AV_AUDIOPOLICY_CFG_PATH)/audio_policy_volumes.xml:$(TARGET_COPY_OUT_VENDOR)/etc/audio_policy_volumes.xml \
    $(FW_AV_AUDIOPOLICY_CFG_PATH)/default_volume_tables.xml:$(TARGET_COPY_OUT_VENDOR)/etc/default_volume_tables.xml \
    $(FW_AV_AUDIOPOLICY_CFG_PATH)/surround_sound_configuration_5_0.xml:$(TARGET_COPY_OUT_VENDOR)/etc/surround_sound_configuration_5_0.xml

PRODUCT_COPY_FILES += \
    frameworks/native/data/etc/android.hardware.keystore.app_attest_key.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.keystore.app_attest_key.xml

# "Audio policy no longer supports legacy .conf configuration format"
USE_XML_AUDIO_POLICY_CONF := 1

# Needed, otherwise SystemServiceRegistry complains about no service being
# published for appwidget.
PRODUCT_COPY_FILES += \
    $(call add-to-product-copy-files-if-exists,\
      frameworks/native/data/etc/android.software.app_widgets.xml:system/etc/permissions/android.software.app_widgets.xml \
      frameworks/native/data/etc/android.hardware.wifi.xml:system/etc/permissions/android.hardware.wifi.xml)

# Add xml file which indicates that the device includes ethernet.
PRODUCT_COPY_FILES += \
    frameworks/native/data/etc/android.hardware.ethernet.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.ethernet.xml

# Avoids some warnings in surfaceflinger.
PRODUCT_PROPERTY_OVERRIDES += \
    ro.sf.lcd_density=384 \
    ro.input.enabled=0

# "Enabling WITH_DEXPREOPT_BOOT_IMG_AND_SYSTEM_SERVER_ONLY pre-optimizes only
# the boot classpath and system server jars."
# See https://source.android.com/devices/tech/dalvik/configure
WITH_DEXPREOPT_BOOT_IMG_AND_SYSTEM_SERVER_ONLY := false

# https://source.android.com/devices/tech/ota/apex#configuring_system_to_support_apex_updates
$(call inherit-product, $(SRC_TARGET_DIR)/product/updatable_apex.mk)

# https://source.android.com/devices/tech/ota/dynamic_partitions/implement
# https://source.android.com/devices/tech/ota/dynamic_partitions/implement#adb-remount
PRODUCT_USE_DYNAMIC_PARTITIONS := false

# https://source.android.com/devices/tech/dalvik/art-class-loader-context#migration-path
PRODUCT_BROKEN_VERIFY_USES_LIBRARIES := true

PRODUCT_COPY_FILES += \
    $(LOCAL_PATH)/init.common.rc:root/init.common.rc \
    $(LOCAL_PATH)/ueventd.rc:$(TARGET_COPY_OUT_VENDOR)/etc/ueventd.rc


PRODUCT_PROPERTY_OVERRIDES += \
    debug.sf.nobootanimation=1

PRODUCT_REQUIRES_INSECURE_EXECMEM_FOR_SWIFTSHADER := true

# Disable OMX
PRODUCT_PROPERTY_OVERRIDES += \
	vendor.media.omx=0 \

KERNEL_MODS := $(wildcard $(LOCAL_PATH)/kernel_modules/*.ko)
ifneq ($(KERNEL_MODS),)
    BOARD_VENDOR_KERNEL_MODULES += $(KERNEL_MODS)
endif
