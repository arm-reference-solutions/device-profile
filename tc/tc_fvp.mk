#
# Copyright (C) 2020 The Android Open-Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

PRODUCT_NAME := tc_fvp
PRODUCT_MANUFACTURER := Arm
PRODUCT_DEVICE := tc_fvp

$(call inherit-product, $(LOCAL_PATH)/device.mk)

PRODUCT_COPY_FILES += \
    device/generic/goldfish/audio/policy/audio_policy_configuration.xml:$(TARGET_COPY_OUT_VENDOR)/etc/audio_policy_configuration.xml \
    device/generic/goldfish/audio/policy/primary_audio_policy_configuration.xml:$(TARGET_COPY_OUT_VENDOR)/etc/primary_audio_policy_configuration.xml \
    frameworks/av/services/audiopolicy/config/a2dp_in_audio_policy_configuration_7_0.xml:$(TARGET_COPY_OUT_VENDOR)/etc/a2dp_in_audio_policy_configuration_7_0.xml \
    frameworks/av/services/audiopolicy/config/bluetooth_audio_policy_configuration_7_0.xml:$(TARGET_COPY_OUT_VENDOR)/etc/bluetooth_audio_policy_configuration_7_0.xml \
    frameworks/av/services/audiopolicy/config/r_submix_audio_policy_configuration.xml:$(TARGET_COPY_OUT_VENDOR)/etc/r_submix_audio_policy_configuration.xml \
    frameworks/av/services/audiopolicy/config/audio_policy_volumes.xml:$(TARGET_COPY_OUT_VENDOR)/etc/audio_policy_volumes.xml \
    frameworks/av/services/audiopolicy/config/default_volume_tables.xml:$(TARGET_COPY_OUT_VENDOR)/etc/default_volume_tables.xml \
    frameworks/av/media/libeffects/data/audio_effects.xml:$(TARGET_COPY_OUT_VENDOR)/etc/audio_effects.xml \


ifeq (${TC_GPU},true)
include $(LOCAL_PATH)/tc_mali.mk 
PRODUCT_COPY_FILES += \
  $(LOCAL_PATH)/fstab/fstab.fvp_mali:$(TARGET_COPY_OUT_RAMDISK)/fstab.total_compute \
  $(LOCAL_PATH)/fstab/fstab.fvp_mali:$(TARGET_COPY_OUT_VENDOR)/etc/fstab.total_compute
else
include $(LOCAL_PATH)/tc_no_mali.mk 
PRODUCT_COPY_FILES += \
  $(LOCAL_PATH)/fstab/fstab.fvp_no_mali:$(TARGET_COPY_OUT_RAMDISK)/fstab.total_compute \
  $(LOCAL_PATH)/fstab/fstab.fvp_no_mali:$(TARGET_COPY_OUT_ROOT)/fstab.total_compute
endif
