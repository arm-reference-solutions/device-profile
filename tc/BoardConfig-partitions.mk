#
# Copyright (C) 2019-2023 The Android Open-Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

TARGET_USERIMAGES_USE_EXT4 := true
# We need normal images, not sparse images.
TARGET_USERIMAGES_SPARSE_EXT_DISABLED := true

# See https://source.android.com/devices/bootloader/system-as-root
BOARD_BUILD_SYSTEM_ROOT_IMAGE := false
BOARD_USES_RECOVERY_AS_BOOT := false

# Workaround: these lines are just to please fs_config_generator.py
# which is passed "--all-partitions $(fs_config_generate_extra_partition_list)".
# The variable would be empty otherwise triggering an argument parsing error.
# OEMIMAGE has no other side-effects in the build system, hope it stays so.
# See build/make/tools/fs_config/Android.mk for details.
BOARD_USES_OEMIMAGE := true
BOARD_OEMIMAGE_FILE_SYSTEM_TYPE := ext4

BOARD_SYSTEMIMAGE_PARTITION_SIZE := 1342177280   # 1.25 GB
BOARD_USERDATAIMAGE_PARTITION_SIZE := 576716800  # 550 MB

  # Choose smaller partition sizes for nanodroid builds.
ifeq (,$(filter %_nano, $(TARGET_PRODUCT)))
    BOARD_SYSTEMIMAGE_PARTITION_SIZE := 3221225472   # 3 GB
    BOARD_USERDATAIMAGE_PARTITION_SIZE := 5368709120  # 5 GB

else
    BOARD_SYSTEMIMAGE_PARTITION_SIZE := 1717986918   # 1.6 GB
    BOARD_USERDATAIMAGE_PARTITION_SIZE := 1073741824  # 1 GB
endif

ifeq (${TC_GPU},true)
    TARGET_COPY_OUT_VENDOR := vendor
    BOARD_VENDORIMAGE_FILE_SYSTEM_TYPE := ext4
    BOARD_VENDORIMAGE_PARTITION_SIZE ?= 536870912 # 512 MiB
endif

# Boot image is only built in user and userdebug modes, In these modes,
# the kernel Image is copied to device profile and used to build the boot image.
# The boot image is signed during Android Verified Boot process.
ifneq ($(TARGET_BUILD_VARIANT),eng)
TARGET_NO_BOOTLOADER := true
TARGET_NO_KERNEL := false
TARGET_NO_RECOVERY := true

TARGET_PREBUILT_KERNEL := device/arm/tc/Image
BOARD_MKBOOTIMG_ARGS := --base 0x0 --kernel_offset 0x80080000 --ramdisk_offset 0x88000000

BOARD_BOOTIMAGE_PARTITION_SIZE := 41943040   # 40 MB
endif

# Not meaningful but some macros assume this value exists
BOARD_FLASH_BLOCK_SIZE := 512

# Left here as a reminder.
# Setting 'BOARD_EXT4_SHARE_DUP_BLOCKS' would make "adb remount" impossible.
# https://android.googlesource.com/platform/system/core.git/+/a962ec0c736ec954aed9daf40bdc06be05cc9eed
# IMPORTANT! Do NOT set this.
# There is a mismatch in APIs and the value doesn't matter.
# See https://android.googlesource.com/platform/build/+/refs/tags/android-10.0.0_r2/tools/releasetools/build_image.py#288
# BOARD_EXT4_SHARE_DUP_BLOCKS := false
