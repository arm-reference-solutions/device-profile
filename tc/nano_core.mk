#
# Copyright (C) 2017 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

# This is based on build/target/product/embedded.mk, with an even smaller
# set of dependencies.

PRODUCT_COPY_FILES += \
    $(LOCAL_PATH)/init.common.rc:root/init.common.rc \
    $(LOCAL_PATH)/ueventd.rc:$(TARGET_COPY_OUT_VENDOR)/etc/ueventd.rc

$(call inherit-product, $(SRC_TARGET_DIR)/product/updatable_apex.mk)

PRODUCT_PACKAGES += \
    com.android.runtime \
    crash_dump \
    gdbserver \
    grep \
    init_vendor \
    ip \
    ping \
    selinux_policy_nonsystem \

# Required host packages
PRODUCT_HOST_PACKAGES += \
    fastboot

# Set a default PRODUCT_BRAND (like in build/target/product/core_tiny.mk for
# instance).
PRODUCT_BRAND := nano

# The default setting is to support 32-bit apps only.
TARGET_SUPPORTS_64_BIT_APPS := true

# Never build test modules, as they pull a huge number of dependencies.
ANDROID_NO_TEST_CHECK := true

# Don't use the precollected boot image profile for generating boot image
PRODUCT_USE_PROFILE_FOR_BOOT_IMAGE := false

# The check would fail because there are no boot jars.
SKIP_BOOT_JARS_CHECK ?= true
